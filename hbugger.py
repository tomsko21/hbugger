from subprocess import Popen, PIPE, STDOUT

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


FILE = "10post4.hs"
FILE_CONTENT = None
FUNCTION_TO_DEBUG = ""

MAXIMUM_STEPS = 1000

def debug_stepProcess():
    global FILE_CONTENT
    with open(FILE, 'r') as file:
        FILE_CONTENT = file.read().split("\n")


    p = Popen(['ghci', FILE], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    p.stdin.write(str.encode(":step "+FUNCTION_TO_DEBUG+" \n"))

    for _ in range(MAXIMUM_STEPS):
        p.stdin.write(str.encode(":step  \n"))

    p.stdin.close()


    OUTPUT_LOG = str((p.stdout.read())).replace("\\n","\n").replace("ghci> not stopped at a breakpoint\n","").replace("\n  "," ")
    process_debug_log(OUTPUT_LOG)

def getFunctionFromLine(line: int):

    function_begin, function_end = 0, 0

    i = line
    while FILE_CONTENT[i] != "":
        i -= 1
    function_begin = i+1

    i = line
    while FILE_CONTENT[i] != "":
        i += 1
    function_end = i - 1

    function_text = ""

    for i in range(function_begin, function_end+1):
        function_text+=FILE_CONTENT[i]+"\n"

    return function_text

def process_debug_log(log):
    print(log)

    print("\n"*50 + "Debugging "+FUNCTION_TO_DEBUG)
    input("Enter to continue...")

    debug_started = False

    for line in log.split("\n"):
        line = line.replace("   ","")
        if "Stopped in" in line:

            # wait for user to jump
            input("Enter to continue...")


            code_location = line.split("Stopped in")[1].split(FILE+":")[1]
            print("?>>",code_location)

            if ":" in code_location:
                line_loc = int(code_location.split(":")[0])
                if "-" in code_location.split(":")[1]:
                    chars_from, chars_to = int(code_location.split(":")[1].split("-")[0]), int(code_location.split(":")[1].split("-")[1])
                else:
                    chars_from = chars_to = int(code_location.split(":")[1])
                code = FILE_CONTENT[line_loc-1][chars_from-1:chars_to]
            else:
                line_loc_start = int(code_location.split("-")[0].replace("(","").replace(")","").split(",")[0])
                chars_from = int(code_location.split("-")[0].replace("(","").replace(")","").split(",")[1])

                line_loc_end = int(code_location.split("-")[1].replace("(", "").replace(")", "").split(",")[0])
                chars_to = int(code_location.split("-")[1].replace("(", "").replace(")", "").split(",")[1])

                code = FILE_CONTENT[line_loc_start - 1][chars_from - 1:]+"\n"

                for l in range(line_loc_start+1,line_loc_end+1):
                    code += FILE_CONTENT[l-1]

                code += FILE_CONTENT[line_loc_end - 1][:chars_to]+"\n"

                line_loc = line_loc_start


            print("\n"*50+"")
            print(getFunctionFromLine(line_loc-1).replace(code,bcolors.OKBLUE+code+bcolors.ENDC))

            debug_started = True
        else:
            if debug_started:

                if "ghci>" in line:
                    print("FUNCTION ENDED: ", bcolors.FAIL+line.split("ghci>")[1]+bcolors.ENDC)
                    print("  ... note, function output in step editor can be broken. Check ghci for accurate output")
                    break

                if not "::" in line or not "=" in line:
                    print(line)
                    continue
                variable = line.split("::")[0]
                type = line.split("::")[1][0:line.split("::")[1].rindex("=")]
                result = line.split("::")[1][line.split("::")[1].rindex("="):]

                variable = bcolors.OKGREEN+variable+bcolors.ENDC if "_result" not in variable \
                    else bcolors.FAIL+variable+bcolors.ENDC

                type = bcolors.OKCYAN+type+bcolors.ENDC

                result = bcolors.BOLD+result+bcolors.ENDC
                print(variable+" :: "+type+" "+result)


function = input("Wanna debug by saved parms [press enter] or specific function: ")
if function != "":
    args = input("Arguments: ")
    FUNCTION_TO_DEBUG = function+" "+args
    debug_stepProcess()
else:
    debug_stepProcess()
